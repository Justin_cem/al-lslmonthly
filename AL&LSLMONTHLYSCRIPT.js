/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 *
 */
define(['N/log', 'N/search','N/record', 'N/file','N/format'],
    /**
     * @param {log} log
     * @param {search} search
     * @param {record} record
     * @param {file} file
     * @param {format} format

     //* @param {MyUtility} _Utility
     */

    /**
     * @returns {Object}
     *
     */
    function(log, search, record,file, format,  _Utility) {

        /**
         * Definition of the Scheduled script trigger point.
         *
         * @param {Object} scriptContext
         * @param {string} scriptContext.type - The context in which the script is executed. It is one of the values from the scriptContext.InvocationType enum.
         * @Since 2015.2
         */
        function execute(scriptContext) {
            //last day of last month - posting date
            var lastDate = new Date();
            //lastDate.setDate() //Add this back when GOLIVE as this script will run after EOM
            /**@type {String}*/
            var exportFileNumber ;

            // search 1 : Employees and al & LSL Balance + Other Details
            /**@type {employeeDetails[]}*/
            var employeeDetailsList = [];

            var employeeDetailFile = file.create({
                name: 'EOM ' + lastDate + ' - Employee AL & LSL Balances',
                fileType: file.Type.CSV,
                encoding: file.Encoding.UTF_8,
                contents: "AL & LSL Month End Balances",
                folder: 593876,
                isOnline: true
            });
            /**{string}*/
            var fileHeader = "Employee Name" + "," + "Hourly Rate" + "," + "AL Hours" + "," + "LSL Hours" + "," + "Available LSL" + "," + "Subsidiary" + "," + "Department" + "," + "Award" + "," + "Years of Service";
            employeeDetailFile.appendLine({value: fileHeader});
            /** @type {(search.Filter[]|String[]|String[][])} */
            var filter = [];
            filter.push(['isinactive', 'IS', 'F']);
            filter.push('and', ['custentity_employment_status', 'NONEOF', 6]);

            /** @type {(search.Filter[]|String[]|String[][])} */
            var c = [
                'entityid',
                'custentity_pr_hourly_rate',
                'custentity_pr_annual_leave_accrued',
                'custentity_pr_lsl_accrued',
                'subsidiary',
                'department',
                'custentity_pr_award_name',
                'custentity_pr_years_of_service',
                'custentity_pr_lsl_available'
            ];
            /**@type {search.Search}*/
            var s = search.create({type: 'employee', filters: filter, columns: c});
            /** @type {string[]}*/
            s.run().each(
                function (row) {
                    //need an array of arrays in here to hold employee info
                    /**@type {employeeDetails}*/
                    var emp = {
                        employeeName: row.getValue({name: 'entityid'}),
                        hourlyRate: row.getValue({name: 'custentity_pr_hourly_rate'}),
                        alHours: row.getValue({name: 'custentity_pr_annual_leave_accrued'}),
                        lslHours: row.getValue({name: 'custentity_pr_lsl_accrued'}),
                        availableLSL: row.getValue({name: 'custentity_pr_lsl_available'}),
                        employeeSubsidiary: row.getValue({name: 'subsidiary'}),
                        employeeDepartment: row.getValue({name: 'department'}),
                        employeeAward: row.getValue({name: 'custentity_pr_award_name'}),
                        yearsOfService: row.getValue({name: 'custentity_pr_years_of_service'})
                    };
                    /**{string}*/
                    var empString = emp.employeeName + "," +
                        emp.hourlyRate.toString() + "," +
                        emp.alHours.toString() + "," +
                        emp.lslHours.toString() + "," +
                        emp.availableLSL.toString() + "," +
                        emp.employeeSubsidiary + "," +
                        emp.employeeDepartment + "," +
                        emp.employeeAward + "," +
                        emp.yearsOfService.toString();

                    employeeDetailsList.push(emp);
                    employeeDetailFile.appendLine({
                        value: empString
                    });
                    return true;
                });
            //log.debug({title: "non terminated emp", details:employeeDetailsList});
            employeeDetailFile.save();
            //still need to sort into seperate entities and departments.
            // search 2 : Custom record Search for Expense and Accrual Accounts
            /**@type {accountForJournal[]}*/
            var expenseAndAccrualAccounts = [];
            /** @type {(search.Filter[]|String[]|String[][])} */
            var f = [];
            f.push(['isinactive', 'IS', 'F']);


            /** @type {(search.Column[]|String[]|String[][])} */
            var c = [
                'custrecord_cem_al_lsl_award',
                'custrecord_cem_al_lsl_subsidiary',
                'custrecord_cem_al_lsl_department',
                'custrecord_cem_al_lsl_al_expense',
                'custrecord_cem_al_lsl_lsl_expense_accoun',
                'custrecord_cem_al_lsl_al_accrual_account',
                'custrecord_cem_al_lsl_current_lsl_accr',
                'custrecord_cem_al_lsl_non_current_lsl_ac'

            ];
            /** @type {search.Search}*/
            var s = search.create({type: 'customrecord_cem_al_lsl_accounts_by_awar', filters: f, columns: c});


            /** @type {String[]}*/
            s.run().each(
                /**
                 * @param {search.result} row
                 * @returns {Boolean}
                 */
                function (row) {
                    /**@type {accountForJournal}*/
                    var accList = {
                        awardName: row.getText({name: 'custrecord_cem_al_lsl_award'}),
                        awardNumber: row.getValue({name: 'custrecord_cem_al_lsl_award'}),
                        subsidiaryNumber: row.getValue({name: 'custrecord_cem_al_lsl_subsidiary'}),
                        departmentNumber: row.getValue({name: 'custrecord_cem_al_lsl_department'}),
                        annualLeaveExpenseAccount: row.getValue({name: 'custrecord_cem_al_lsl_al_expense'}),
                        lslExpenseAccount: row.getValue({name: 'custrecord_cem_al_lsl_lsl_expense_accoun'}),
                        annualLeaveAccrualAccount: row.getValue({name: 'custrecord_cem_al_lsl_al_accrual_account'}),
                        currentLSLAccrualAccount: row.getValue({name: 'custrecord_cem_al_lsl_current_lsl_accr'}),
                        nonCurrentLSLAccrualAccount: row.getValue({name: 'custrecord_cem_al_lsl_non_current_lsl_ac'})

                    };
                    expenseAndAccrualAccounts.push(accList);


                    return true;
                }
            );
            log.debug({title: 'new account', details: expenseAndAccrualAccounts});

            //   todo 3  get balance sheet values // Search 4 : AL & LSL Accrual Balance at EOM
            //get subsidiary list
            /** @type {subsidiaryNumber[]}*/
            var subsidiaryNumbers = [];
            /** @type {(search.Filter[]|String[]|String[][])} */
            var filter = [];
            filter.push(['ISINACTIVE', 'IS', 'F']);
            /** @type {(search.Filter[]|String[]|String[][])} */
            var c = [];
            c.push({name: 'name'});
            /**@type {search.Search}*/
            var s = search.create({type: 'subsidiary', filters: filter, columns: c});
            /** @type {string[]}*/
            s.run().each(
                function (row) {
                    if (row.id !== "5") {
                        /** @type {subsidiaryNumber}*/
                        var sN = {
                            name: row.getValue({name: 'name'}),
                            internalId: row.id
                        };

                        subsidiaryNumbers.push(sN);
                        return true;
                    }
                });

            // log.debug({title: "subsidiary", details: subsidiaryNumbers});
            /** {subsidiaryNumber[]}*/
            var departmentNumbers = [];
            /** @type {(search.Filter[]|String[]|String[][])} */
            var filter = [];
            filter.push(['ISINACTIVE', 'IS', 'F']);
            /** @type {(search.Filter[]|String[]|String[][])} */
            var c = [];
            c.push({name: 'name'});
            /**@type {search.Search}*/
            var s = search.create({type: 'department', filters: filter, columns: c});
            /** @type {string[]}*/
            s.run().each(
                function (row) {

                    /** @type {subsidiaryNumber}*/
                    var sN = {
                        name: row.getValue({name: 'name'}),
                        internalId: row.id
                    };

                    if (sN.name.indexOf('Properties') !== -1
                        || sN.name.indexOf('Early') !== -1
                        || sN.name.indexOf('Campus') !== -1
                    ) {
                        return true;
                    } else {
                        departmentNumbers.push(sN);
                    }
                    return true;

                });
            /** {string[]}*/
            var awardNumbers = [];
            var distinct = [];
            employeeDetailsList.forEach(function (e) {
                if (!distinct[e.employeeAward]) {
                    awardNumbers.push(e.employeeAward)
                    distinct[e.employeeAward] = 1;
                }
            });


            /** {subsidiaryNumber[]}*/
            var awardNumberSearch = [];
            /** @type {(search.Filter[]|String[]|String[][])} */
            var filter = [];
            filter.push(['ISINACTIVE', 'IS', 'F']);
            /** @type {(search.Filter[]|String[]|String[][])} */
            var c = [];
            c.push({name: 'name'});
            /**@type {search.Search}*/
            var s = search.create({type: 'customrecord_pr_modern_awards', filters: filter, columns: c});
            /** @type {string[]}*/
            s.run().each(
                function (row) {

                    /** @type {subsidiaryNumber}*/
                    var sN = {
                        name: row.getValue({name: 'name'}),
                        internalId: row.id,
                        awardType: ""
                    };
                    //awardNumberSearch.push(sN);

                    awardNumbers.forEach(function (aN) {

                        //log.debug({title:'before testsN', details:  sN});
                        //log.debug({title:'before testaN', details:  aN});
                        if (aN === sN.internalId) {
                            // log.debug({title:'snMatch', details: sN});
                            awardNumberSearch.push(sN);
                        }
                        return true;
                    });


                    return true;

                });

            //  log.debug({title: "awards", details: awardNumberSearch});
            //log.debug({title: "departmentNumbers", details: departmentNumbers});
            //get account balance


            /**@type {accountBalance[]}*/
            var accountBalances = [];
            /**@type {Number}*/
            var dd = lastDate.getDate() + (31 - 26);
            /**@type {Number}*/
            var mm = lastDate.getMonth() + 1;
            /**@type {Number}*/
            var yyyy = lastDate.getFullYear();
            /**@type {String}*/
            var bDate = dd.toString() + "/" + mm.toString() + "/" + yyyy.toString();

            /**@type {number[]}*/
            var accrualAccountNumber = [];
            expenseAndAccrualAccounts.forEach(function (settingAccount) {
                accrualAccountNumber.push(settingAccount.annualLeaveAccrualAccount);
                accrualAccountNumber.push(settingAccount.currentLSLAccrualAccount);
                accrualAccountNumber.push(settingAccount.nonCurrentLSLAccrualAccount);

            });

            subsidiaryNumbers.forEach(
                /**
                 *
                 * @param sN
                 */
                function (sN) {


                    /** @type {(search.Filter[]|String[]|String[][])} */
                    var filter = [];

                    filter.push(['Account', 'ANYOF', accrualAccountNumber]);


                    filter.push('and', ['trandate', 'NOTAFTER', bDate]);
                    filter.push('and', ['subsidiary', 'IS', sN.internalId]);

                    /** @type {(search.Filter[]|String[]|String[][])} */
                    var c = [{
                        name: 'Amount',
                        summary: search.Summary.SUM
                    }, {
                        name: 'Subsidiary',
                        summary: search.Summary.GROUP
                    }, {
                        name: 'Account',
                        summary: search.Summary.GROUP
                    }, {
                        name: 'department',
                        summary: search.Summary.GROUP

                    }];
                    /**@type {search.Search}*/
                    var s = search.create({type: 'transaction', filters: filter, columns: c});
                    /** @type {string[]}*/
                    s.run().each(
                        /**
                         *
                         * @param row
                         * @returns {boolean}
                         */
                        function (row) {
                            /**@type {accountBalance}*/
                            var accountBalanceTemp = {
                                eomBalance: row.getValue({name: 'Amount', summary: 'SUM'}),
                                subsidiaryNumber: row.getValue({name: 'Subsidiary', summary: 'GROUP'}),
                                subsidiaryName: row.getText({name: 'Subsidiary', summary: 'GROUP'}),
                                departmentNumber: row.getValue({name: 'department', summary: 'GROUP'}),
                                departmentName: row.getText({name: 'department', summary: 'GROUP'}),
                                accountNumber: row.getValue({name: 'Account', summary: 'GROUP'}),
                                accountName: row.getText({name: 'Account', summary: 'GROUP'})
                            };


                            accountBalances.push(accountBalanceTemp);
                            log.debug({title: 'balance',details:accountBalanceTemp});
                            return true
                        });

                    return true;

                });
            //log.debug({title: 'balance',details:accountBalanceTemp});


            //log.debug({title: 'accountBalances', details: accountBalances});
            //log.debug({title: 'date', details: lastDate});
            //log.debug({title: "bdate", details: bDate});

            //   todo 4 calculate
            /**
             *
             * @param {string} subsidiaryId
             * @param {string} departmentId
             * @param {string} awardId
             * @param {string} leaveType
             * @param {string} leaveIdentifier
             * @returns {number}
             */
            function calculateBalance(subsidiaryId, departmentId, awardId, leaveType, leaveIdentifier) {
                /** @type {employeeDetails []}*/
                var filteredEmployees = employeeDetailsList.filter(function (e) {
                    return e.employeeSubsidiary == subsidiaryId &&
                        e.employeeDepartment == departmentId &&
                        e.employeeAward == awardId
                });
                if (leaveIdentifier === "Non Current") {
                    filteredEmployees = filteredEmployees.filter(function (e) {
                        return e.yearsOfService >= 5.00 && e.yearsOfService < 9.00
                    })
                } else if (leaveIdentifier === "Current") {
                    filteredEmployees = filteredEmployees.filter(function (e) {
                        return e.yearsOfService >= 9.00
                    })
                } else if (leaveIdentifier === "Annual") {
                    filteredEmployees = filteredEmployees;
                }


                var totalHours = 0;
                filteredEmployees.forEach(function (employee) {
                    if ((employee[leaveType] > 0 && employee.hourlyRate > 0)) {
                        // LSLHours is total Balance of LSL.
                        if (leaveIdentifier === "Non Current") {
                            var nonCurrentLSLHours = employee.lslHours;
                            var nonCurrentLSLHoursInDollarValue = nonCurrentLSLHours * employee.hourlyRate;
                            totalHours = totalHours + (1.095 * nonCurrentLSLHoursInDollarValue);

                        } else if (leaveIdentifier === "Current") {
                            var currentLSLHours = employee.lslHours;
                            var currentLSLHoursInDollarValue = currentLSLHours * employee.hourlyRate;
                            totalHours = totalHours + (1.095 * currentLSLHoursInDollarValue);
                        } else if (leaveIdentifier === "Annual") {

                            var alHoursInDollarValue = employee.hourlyRate * employee[leaveType];
                            totalHours = totalHours + alHoursInDollarValue;

                        }
                        return true;
                    }
                });

                return totalHours;
            }

            //todo need a loop to go through all of the subs, depts and awards and leave types
            //var hoursTotal = calculateBalance("1", "3","123", "alHours"  );
            //Function to return correct
            function getBalanceSheetBalance(subsidiaryNumber, departmentNumber, awardType, provisionType) {
                //log.debug({title:"awardType", details: awardType});
                //log.debug({title:"provisiontype", details: provisionType});
                //log.debug({title:"dep", details: departmentNumber});
                //log.debug({title:"sub", details: subsidiaryNumber});
                var provtypedoublecheck;
                /**@type {accountBalance[]}*/
                var accountBalanceList = accountBalances;
                accountBalanceList = accountBalanceList.filter(function (accountBalanceToCheck) {
                    var acName = accountBalanceToCheck.accountName;
                    var awardTypeCheck = acName.indexOf(awardType);
                    var provTypeCheck = acName.indexOf(provisionType);
                    provtypedoublecheck = provisionType;
                    var provCheck = acName.indexOf('provision');


                    //log.debug({title:"name", details: acName});
                    //log.debug({title:"awardtype", details: awardTypeCheck});
                    //log.debug({title:"provcheck", details: provTypeCheck});

                    return accountBalanceToCheck.subsidiaryNumber === subsidiaryNumber &&
                        accountBalanceToCheck.departmentNumber === departmentNumber &&
                        awardTypeCheck > -1 &&
                        provTypeCheck > -1

                });
                //log.debug({title:"provtype", details:provtypedoublecheck});

                if (accountBalanceList.length > 0) {
                    //log.debug({title:'gotBalanceSheetBalance', details: accountBalanceList });
                    return accountBalanceList[0];
                } else {
                    /**@type {accountBalance}*/
                    var noAccount = {
                        eomBalance: 0,
                        subsidiaryNumber: 0,
                        subsidiaryName: "None",
                        accountNumber: 0,
                        accountName: "None",
                        departmentNumber: 0,
                        departmentName: "None"
                    }
                    accountBalanceList.push(noAccount);
                    //log.debug({title:'is null', details: accountBalanceList });
                    return accountBalanceList[0];
                }
            }

            function checkEomBalanceForJournal(accountBalance, calculatedBalance) {

                /** {number}*/
                var checkedAmount;
                /** {number}*/
                var eomBalance = Number(accountBalance);

                if (eomBalance === calculatedBalance) {
                    checkedAmount = 0;
                } else if (eomBalance > calculatedBalance) {
                    checkedAmount = eomBalance - calculatedBalance;

                } else {
                    // balance to check is < than calculated balance so do below
                    //annaul leave dr expense cr balance
                    checkedAmount = eomBalance - calculatedBalance;
                }
                checkedAmount = checkedAmount.toFixed(2);
                return checkedAmount;
            }



            awardNumberSearch.forEach(function (award) {
                /**@type {String}*/
                var name = award.name;

                if (name.indexOf("Teachers") !== -1) {
                    award.awardType = "Teachers"
                } else if (name.indexOf("General") !== -1) {
                    award.awardType = "General"
                } else if (name.indexOf("Individual") !== -1) {
                    award.awardType = "Individual"
                } else if (name.indexOf("Children") !== -1) {
                    award.awardType = "Children"
                } else if (name.indexOf("Private") !== -1) {
                    award.awardType = "private"
                }


            });
            log.debug({title: 'awardnumbersearch', details: awardNumberSearch});
            var calcExport = file.create({
                name: 'Calculated AL and LSL Balances ' + lastDate,
                fileType: file.Type.CSV,
                encoding: file.Encoding.UTF_8,
                contents: "AL & LSL Calculated balance",
                folder: 593876,
                isOnline: true
            });
            var fileheader = "Subsidiary" + "," + "Department" + "," + "Award" + "," + "Leave Type" + "," + "Calc" + "," + "BS" + "," + "Account" + "," + "Difference";
            calcExport.appendLine({value: fileheader});
            /**@type {infoForRecord[]}*/
            var journalNumbersPerSubsidiary = [];
            /**@type {calculatedAmountForJournal[]}*/
            var amountsCheckedForJournal = [];
            subsidiaryNumbers.forEach(function (subsidiaryNumber) {
                departmentNumbers.forEach(function (department) {
                    awardNumberSearch.forEach(function (award) {
                        //Annual leave
                        /**@type {number}*/
                        var annualLeaveCalculatedBalance = calculateBalance(subsidiaryNumber.internalId, department.internalId, award.internalId, "alHours", "Annual");
                        // Non Current LSL
                        /**@type {number}*/
                        var currentLSLCalculatedBalance = calculateBalance(subsidiaryNumber.internalId, department.internalId, award.internalId, "lslHours", "Current");
                        //Current LSL
                        /**@type {number}*/
                        var nonCurrentLSLCalculatedBalance = calculateBalance(subsidiaryNumber.internalId, department.internalId, award.internalId, "lslHours", "Non Current");
                        //log.debug({title:'sub', details :subsidiaryNumber});
                        //log.debug({title:'dep', details :department});
                        //log.debug({title:'awardid', details :award.internalId});
                        //log.debug({title:'awardtype', details :award.awardType});

                        /**@type {accountBalance}*/
                        var alBalanceToCheck = getBalanceSheetBalance(subsidiaryNumber.internalId, department.internalId, award.awardType, 'Annual');

                        /**@type {accountBalance}*/
                        var currentLSLBalanceToCheck = getBalanceSheetBalance(subsidiaryNumber.internalId, department.internalId, award.awardType, 'LSL Current');

                        /**@type {accountBalance}*/
                        var nonCurrentLSLBalanceToCheck = getBalanceSheetBalance(subsidiaryNumber.internalId, department.internalId, award.awardType, 'LSL Non-Current');


                        /**@type {calculatedAmountForJournal}*/
                        var amountsCheckedForJournalSingle = {
                            alDifference: checkEomBalanceForJournal(alBalanceToCheck.eomBalance, annualLeaveCalculatedBalance),
                            nonCurrentLSLDifference: checkEomBalanceForJournal(nonCurrentLSLBalanceToCheck.eomBalance, nonCurrentLSLCalculatedBalance),
                            currentLSLDifference: checkEomBalanceForJournal(currentLSLBalanceToCheck.eomBalance, currentLSLCalculatedBalance),
                            subsidiaryNumber: subsidiaryNumber.internalId,
                            departmentNumber: department.internalId,
                            awardNumber: award.internalId
                        };

                        /**{number}*/
                        var zeroCheck = 0.00;
                        //log.debug({title:'aldifference', details: amountsCheckedForJournalSingle});
                        if (amountsCheckedForJournalSingle.alDifference !== undefined &&
                            alBalanceToCheck.accountName !== "None"
                        ) {

                            var calcStringAL = subsidiaryNumber.name + "," +
                                department.name + "," +
                                award.name + "," +
                                "AL" + "," +
                                annualLeaveCalculatedBalance.toString() + "," +
                                alBalanceToCheck.eomBalance + "," +
                                alBalanceToCheck.accountName + "," +
                                amountsCheckedForJournalSingle.alDifference.toString();

                            calcExport.appendLine({value: calcStringAL})
                        }
                        if (amountsCheckedForJournalSingle.nonCurrentLSLDifference !== undefined &&
                            nonCurrentLSLBalanceToCheck.accountName !== "None"
                        ) {

                            var calcStringNCLSL = subsidiaryNumber.name + "," +
                                department.name + "," +
                                award.name + "," +
                                "nCLSL" + "," +
                                nonCurrentLSLCalculatedBalance.toString() + "," +
                                nonCurrentLSLBalanceToCheck.eomBalance + "," +
                                nonCurrentLSLBalanceToCheck.accountName + "," +
                                amountsCheckedForJournalSingle.nonCurrentLSLDifference.toString();

                            calcExport.appendLine({value: calcStringNCLSL});
                        }
                        if (amountsCheckedForJournalSingle.currentLSLDifference !== undefined &&
                            currentLSLBalanceToCheck.accountName !== "None"
                        ) {
                            var calcStringCLSL = subsidiaryNumber.name.toString() + "," +
                                department.name + "," +
                                award.name + "," +
                                "CLSL" + "," +
                                currentLSLCalculatedBalance.toString() + "," +
                                currentLSLBalanceToCheck.eomBalance + "," +
                                currentLSLBalanceToCheck.accountName + "," +
                                amountsCheckedForJournalSingle.currentLSLDifference.toString();

                            calcExport.appendLine({value: calcStringCLSL});
                        }


                        /** @type {Number}*/
                        var fileNumber = calcExport.save();
                        exportFileNumber = String(fileNumber);
                        if (amountsCheckedForJournalSingle.alDifference == 0 &&
                            amountsCheckedForJournalSingle.nonCurrentLSLDifference == 0 &&
                            amountsCheckedForJournalSingle.currentLSLDifference == 0) {
                            return true;
                        } else {
                            amountsCheckedForJournal.push(amountsCheckedForJournalSingle);
                        }

                        return true;
                    });


                    //log.debug({title:"calcexport"});
                    //Teacher
                    //General Eduation
                    //childrens services
                    // Independent Contracts


                    return true;
                });
                return true
            });
            log.debug({title: 'calcs', details: amountsCheckedForJournal});
            //Journal in subsidiary
            amountsCheckedForJournal.forEach(function (differenceJournal) {
                var jnlNumber;

                    /**{accountForJournal[]}*/
                    var accountSettingsRecord = expenseAndAccrualAccounts.filter(function (e) {
                        return e.subsidiaryNumber == differenceJournal.subsidiaryNumber &&
                            e.departmentNumber == differenceJournal.departmentNumber &&
                            e.awardNumber == differenceJournal.awardNumber

                    });
                    //only want to do one journal.
                    log.debug({title: 'match account to difference journal', details: accountSettingsRecord});
                    log.debug({title: 'difference journal', details: differenceJournal});
                    //create Advanced Intercoy Journal.
                   /** @type {number}*/
                   var lineCount =0;
                    /** @type {record.Record}*/
                    var r = record.create({
                        type: record.Type.JOURNAL_ENTRY

                    });

                    //Set Header Area
                    r.setValue({fieldId: 'customform', value: 30});
                    r.setValue({fieldId: 'trandate', value: lastDate});
                    r.setValue({fieldId: 'memo', value: 'EOM AL & LSL Adjustment'});
                    //this is removed to ensure it doesnt rocess a journsl. just want to check it can match to the correct accounts.
                    r.setValue({fieldId: 'subsidiary', value: differenceJournal.subsidiaryNumber});
                    r.setValue({fieldId: 'department', value: differenceJournal.departmentNumber});
                    //Annual leave Adjustment - Determine if expense needs to be dr or cr
                    /** @type {calculatedAmountForJournal[]}*/

                    //Annual leave
                    //Annual Leave Expense
                    /** @type {string}*/
                    var memoText = 'Annual Leave Adjustment ' + bDate;
                    /** @type {number}*/
                    var adjustmentAmount = Math.abs(differenceJournal.alDifference);
                    /**@type {string}*/
                    var alExp = String(accountSettingsRecord[0].annualLeaveExpenseAccount);
                    log.debug({title: 'accountString for expense', details: alExp});

                    r.setSublistValue({sublistId: 'line', fieldId: 'account', line: lineCount, value: alExp});

                    if (differenceJournal.alDifference < 0) {
                        r.setSublistValue({sublistId: 'line', fieldId: 'debit', line: lineCount, value: adjustmentAmount})
                    }
                    if (differenceJournal.alDifference > 0) {
                        r.setSublistValue({sublistId: 'line', fieldId: 'credit', line: lineCount, value: adjustmentAmount})
                    }
                    r.setSublistValue({sublistId: 'line', fieldId: 'memo', line: lineCount, value: memoText});
                    r.setSublistValue({
                        sublistId: 'line',
                        fieldId: 'department',
                        line: lineCount,
                        value: differenceJournal.departmentNumber
                    });
                    lineCount +=1;
                        log.debug({title:'Al exp lineCount', details:lineCount});
                    //Annual Leave Liability
                    /**@type {string}*/
                    var alAcr = String(accountSettingsRecord[0].annualLeaveAccrualAccount);
                    r.setSublistValue({sublistId: 'line', fieldId: 'account', line: lineCount, value: alAcr});

                    if (differenceJournal.alDifference < 0) {
                        r.setSublistValue({sublistId: 'line', fieldId: 'credit', line: lineCount, value: adjustmentAmount})
                    }
                    if (differenceJournal.alDifference > 0) {
                        r.setSublistValue({sublistId: 'line', fieldId: 'debit', line: lineCount, value: adjustmentAmount})
                    }
                    r.setSublistValue({sublistId: 'line', fieldId: 'memo', line: lineCount, value: memoText});
                    r.setSublistValue({
                        sublistId: 'line',
                        fieldId: 'department',
                        line: lineCount,
                        value: differenceJournal.departmentNumber
                    });
                    lineCount +=1;
                        log.debug({title:'currentlsls  Expense lineCount', details:lineCount});
                    //Current LSL Liability
                    if (differenceJournal.currentLSLDifference != 0.00) {
                        /**{string}*/
                        var cLSLAcrAc = String(accountSettingsRecord[0].currentLSLAccrualAccount)
                        r.setSublistValue({
                            sublistId: 'line',
                            fieldId: 'account',
                            line: lineCount,
                            value: cLSLAcrAc
                        });
                        r.setSublistValue({
                            sublistId: 'line',
                            fieldId: 'memo',
                            line: lineCount,
                            value: 'Current LSL Adjustment ' + lastDate
                        });

                        if (differenceJournal.currentLSLDifference < 0) {
                            r.setSublistValue({
                                sublistId: 'line',
                                line: lineCount,
                                fieldId: 'credit',
                                value: Math.abs(differenceJournal.currentLSLDifference)
                            })
                        }
                        if (differenceJournal.currentLSLDifference > 0) {
                            r.setSublistValue({
                                sublistId: 'line',
                                line: lineCount,
                                fieldId: 'debit',
                                value: Math.abs(differenceJournal.currentLSLDifference)
                            })
                        }
                        lineCount +=1;
                        log.debug({title:' non Current lsllineCount', details:lineCount});
                    }

                    // Non Current LSL Liability
                    if (differenceJournal.nonCurrentLSLDifference != 0.00) {
                        /**{string}*/
                        var nCLSLAcrAc = String(accountSettingsRecord[0].nonCurrentLSLAccrualAccount)
                        r.setSublistValue({
                            sublistId: 'line',
                            line: lineCount,
                            fieldId: 'account',
                            value: nCLSLAcrAc
                        });
                        r.setSublistValue({
                            sublistId: 'line',
                            line: lineCount,
                            fieldId: 'memo',
                            value: 'Non Current LSL Adjustment ' + lastDate
                        });

                        if (differenceJournal.nonCurrentLSLDifference < 0) {
                            r.setSublistValue({
                                sublistId: 'line',
                                line: lineCount,
                                fieldId: 'credit',
                                value: Math.abs(differenceJournal.nonCurrentLSLDifference)
                            })
                        }
                        if (differenceJournal.nonCurrentLSLDifference > 0) {
                            r.setSublistValue({
                                sublistId: 'line',
                                line: lineCount,
                                fieldId: 'debit',
                                value: Math.abs(differenceJournal.nonCurrentLSLDifference)
                            })
                        }
                        lineCount +=1;
                        log.debug({title:' lsls exp lineCount', details:lineCount});
                    }

                    // LSL Expense
                    /**{number}*/
                    var lslExpenseDifference = parseFloat(differenceJournal.nonCurrentLSLDifference,2) + parseFloat(differenceJournal.currentLSLDifference,2);
                    lslExpenseDifference = lslExpenseDifference.toFixed(2);
                    /**{string}*/
                    var lslExpAc = String(accountSettingsRecord[0].lslExpenseAccount)
                    log.debug({title:'difference in lsl', details:lslExpenseDifference});
                    log.debug({title:'LSL Exp Account',details:lslExpAc});
                    log.debug({title:'beforestring', details:accountSettingsRecord[0].lslExpenseAccount});
                    log.debug({title:'lineCount', details:lineCount});

                    if (lslExpenseDifference != 0.00) {
                        r.setSublistValue({
                            sublistId: 'line',
                            line: lineCount,
                            fieldId: 'account',
                            value: lslExpAc
                        });
                        r.setSublistValue({
                            sublistId: 'line',
                            line: lineCount,
                            fieldId: 'memo',
                            value: 'Current & Non Current LSL Adjustment ' + lastDate
                        });

                        if (lslExpenseDifference < 0) {
                            r.setSublistValue({
                                sublistId: 'line',
                                line: lineCount,
                                fieldId: 'debit',
                                value: Math.abs(lslExpenseDifference)
                            })
                        }
                        if (lslExpenseDifference > 0) {
                            r.setSublistValue({
                                sublistId: 'line',
                                line: lineCount,
                                fieldId: 'credit',
                                value: Math.abs(lslExpenseDifference)
                            })
                        }
                    }

                    //create journal and save number into string - prob need sub number too
                    /**@type {number}*/
                    var jnl = r.save();

                    /**@type {infoForRecord}*/
                    var recordInfo=  {
                        journalNumber : String(jnl),
                        subsidiaryNumber :differenceJournal.subsidiaryNumber,
                        departmentNumber:differenceJournal.departmentNumber,
                        awardNumber:differenceJournal.awardNumber
                         }
                    journalNumbersPerSubsidiary.push(recordInfo);
                    log.debug({title: 'journal', details: journalNumbersPerSubsidiary});


            });


            /**@type {string}*/
            var memoText = "AL LSL Adjustment for " + bDate

            journalNumbersPerSubsidiary.forEach(function(jnlInfo){


            /** @type {record.Record}*/
            var r = record.create({
                type: 'customrecord_cem_al_lsl_adjustment_eom'

            });

            // might need to get different journal reference once journal has been completed. us ID as key to write into thsi record.
            r.setValue({fieldId: 'name',value: memoText});
            r.setValue({fieldId:'custrecord_cem_eom_leave_prov_month_end', value: memoText});
            r.setValue({fieldId:'custrecord_cem_eom_leave_prov_bal_report', value: exportFileNumber});
            r.setValue({fieldId:'custrecord_cem_al_lsl_eom_date', value: lastDate});
            r.setValue({fieldId:'custrecord_cem_al_lsl_sub', value: jnlInfo.subsidiaryNumber});
            r.setValue({fieldId:'custrecord_cem_al_lsl_dep', value: jnlInfo.departmentNumber});
            r.setValue({fieldId:'custrecord_cem_al_lsl_award_id', value: jnlInfo.awardNumber});
            r.setValue({fieldId:'custrecord_cem_al_lsl_jnl', value: jnlInfo.journalNumber});
            r.save();

            });


    }

        return {
            execute: execute
        };

    });
