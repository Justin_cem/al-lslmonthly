/**
 * @NApiVersion 2.x
 * @NScriptType Script
 * @NModuleScope SameAccount
 *
 */
define(['N/log', 'N/search', 'N/record', 'N/file', 'N/format'],
    /**
     * @param {log} log
     * @param {search} search
     * @param {record} record
     * @param {file} file
     * @param {format} format
     //* @param {MyUtility} _Utility
     */

    /**
     * @returns {Object}
     *
     */
    function (log, search, record, file, format, _Utility) {


        function getBalanceSheet(subsidiary, eomDate, bsAccounts) {
            /** @type {(search.Filter[]|String[]|String[][])} */
            var filter = [];
            filter.push(['Account', 'ANYOF', '1473']);
            var eomDate = format.parse({
                value: '30/4/2020',
                type: format.Type.DATE
            });
            filter.push('and', ['trandate', 'NOTAFTER', '30/04/2020']);

            /** @type {(search.Filter[]|String[]|String[][])} */
            var c = [{
                name: 'Amount',
                summary: search.Summary.SUM
            }, {
                name: 'Subsidiary',
                summary: search.Summary.GROUP
            }, {
                name: 'Account',
                summary: search.Summary.GROUP
            }, {
                name: 'department',
                summary: search.Summary.GROUP

            }];
            /**@type {search.Search}*/
            var s = search.create({type: 'transaction', filters: filter, columns: c});
            /** @type {string[]}*/
            s.run().each(
                function (row) {
                    log.debug({title: "BS", details: row});
                    return true;
                });
        }

        return {
            execute: []
        };
            /**@namespace   getBalanceSheet*/
    });