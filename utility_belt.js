/**
 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 */
define(['N/format', 'N/log'],

    /**
     * @param {format} format
     * @param {log} log
     */
    function(format, log) {

        /**
         * Converts a String to Number (if needed).
         * @param {String|Number|Object} s
         * @returns {?Number}
         */
        function toNumber(s) {
            if (typeof s === "number") return s;
            if (typeof s === "string" && !isNaN(parseFloat(s))) return parseFloat(s);
            if (typeof s === "object" && s && s.hasOwnProperty('value')) return toNumber(s.value);
            return null;
        }

        /**
         * Converts the Javascript Date to a Netsuite suitable string for use in a saved search
         * @param {Date} d
         * @returns {String}
         */
        function nsDate(d) {
            if (isDate(d)) {
                /** @type {Number} */
                var y = d.getFullYear() > 2000 ? d.getFullYear() - 2000 : d.getFullYear() - 1900;
                return "".concat(d.getDate().toString(), "/", zeroPad(d.getMonth() + 1, 2), "/", y.toString());
            }
            return d.toString();
        }

        /**
         * Uses NetSuite's format.parse function to format a string into Date
         * @param {String} s
         * @returns {?Date}
         */
        function fDate(s) {
            if (s === null || s === '') return null;
            /** @type {Date} */
            var d = format.parse({value: s, type: format.Type.DATE});
            if (isDate(d)) return d;
            s = s.replace(/\D/g, '/');
            try {
                /** @type {Date} */
                d = new Date(s);
                if (isDate(d)) return d;
            } catch(err) {
                return null;
            }
            return null;
        }

        /**
         * Creates a new time of day object
         * @param {String} s - time in hh:mm AM|PM format, eg. "07:42 AM" or "07:42" (without AM/PM we
         *                      assume 24 hour time)
         * @returns {?TimeOfDay}
         */
        function fTimeOfDay(s) {
            /** @type {String[]} */
            var t = s.match(/(\d+)/g);
            if (t && t.length > 1) {
                /** @type {Number} */
                var hr = toNumber(t[0]);
                /** @type {Number} */
                var min = toNumber(t[1]);
                /** @type {Number} */
                var sec = t.length > 2 ? toNumber(t[2]) : 0;
                /** @type {Boolean} */
                var is24Hour = !s.match(/(am|pm)/gi);
                /** @type {Boolean} */
                var isAM = !is24Hour && !!s.match(/(am)/gi);
                /** @type {Number} */
                var h24 = is24Hour ? hr : isAM ? (hr === 12 ? 0 : hr) : (hr === 12 ? 12 : hr + 12);
                /** @type {Number} */
                var as2400 = (h24 * 100) + min;
                return {
                    hour: hr,
                    hour24: h24,
                    minute: min,
                    seconds: sec,
                    isAM: isAM,
                    as2400: as2400
                };
            }
            return null;
        }

        /**
         * Checks if value is date
         * @param {Date|String|Number|null|Object} d
         * @return {Boolean}
         * @source https://stackoverflow.com/questions/2831345/is-there-a-way-to-check-if-a-variable-is-a-date-in-javascript
         */
        function isDate(d) {
            return d instanceof Date && !isNaN(d);
        }

        /**
         * Checks if two dates are the same (doesn't compare time and timezone)
         * @param {Date} d1
         * @param {Date} d2
         * @returns {Boolean}
         */
        function sameDates(d1, d2) {
            if (isDate(d1) && isDate(d2)) {
                return d1.getFullYear() === d2.getFullYear() && d1.getMonth() === d2.getMonth() && d1.getDate() === d2.getDate();
            }
            return false;
        }

        /**
         * Checks if two dates are the same date and time (compares hours, minutes and seconds)
         * @param {Date} d1
         * @param {Date} d2
         * @param {Boolean} [excludeSeconds] - if you'd like to exclude seconds check
         * @returns {Boolean}
         */
        function sameDateTimes(d1, d2, excludeSeconds) {
            excludeSeconds = excludeSeconds == null ? false : excludeSeconds;
            if (isDate(d1) && isDate(d2)) {
                if (sameDates(d1, d2)) {
                    if (excludeSeconds) {
                        return d1.getHours() === d2.getHours() && d1.getMinutes() === d2.getMinutes();
                    }
                    return d1.getHours() === d2.getHours() && d1.getMinutes() === d2.getMinutes() && d1.getSeconds() === d2.getSeconds();
                }
            }
            return false;
        }

        /**
         * Zero pad string
         * @param {Number} n - the number to pad with zeroes (if lacking significant numbers)
         * @param {Number} sig - number of significant numbers, eg. 4 for 270, would result in 0270
         * @returns {String}
         */
        function zeroPad(n, sig) {
            /** @type {String} */
            var s = n.toString();
            if (s.length >= sig) return s;
            while (s.length < sig) {
                s = "0".concat(s);
            }
            return s;
        }

        /**
         * Format a yyyy-mm-dd string value into a Javascript Date value
         * @param {String} s
         * @returns {Date}
         */
        function sDate(s) {
            /** @type {String[]} */
            var n = s.match(/(\d+)/g);
            /** @type {Number} */
            var y = toNumber(n[0]);
            /** @type {Number} */
            var m = toNumber(n[1]) - 1;
            /** @type {Number} */
            var d = toNumber(n[2]);
            return new Date(y, m, d);
        }

        /**
         * Converts a CSV line into an array of strings
         * @param {String} line
         * @returns {String[]}
         */
        function getCsvLine(line) {
            /**
             * @type {String[]}
             * @link https://stackoverflow.com/a/11457952/1618944
             */
            var arr = line.match(/(".*?"|[^",]+)(?=\s*,|\s*$)/g);
            arr = arr || [];
            // clean array values of the escaping character
            arr = arr.map(
                /**
                 * @param {String} el
                 * @returns {String}
                 */
                function (el) {
                    return el.replace(/[\\"]/g, "");
                }
            );
            return arr;
        }

        /**
         * Adds to the prototype chain a method for converting strings to Proper Case.
         * Eg. RyAN shEEHy-sMiTh => Ryan Sheehy-Smith
         * @param {String} name
         * @param {?Boolean=false} [isSurname] - default is false
         * @returns {String}
         */
        function toProperCase(name, isSurname) {
            isSurname = isSurname ? isSurname : false;
            /** @type {String} */
            var result = name.replace(/(\w+)/g, function(txt) {
                /** @type {String} */
                var caps = txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
                /** @type {RegExp} checking if the surname contains Mc */
                var re = /(Mc)(.+)/;
                if (isSurname && caps.match(re)) {
                    return caps.replace(re, function() {
                        /** @type {String} */
                        var preMac = arguments[1];
                        /** @type {String} */
                        var postMac = arguments[2];
                        return preMac + postMac.charAt(0).toUpperCase() + postMac.substr(1).toLowerCase();
                    });
                }
                return caps;
            });
            return result.trim();
        }

        /**
         * Converts a date into a string, using mmm-yyyy format.
         * @example 1/02/2019 = Feb-2019
         * @param {Date} d
         * @returns {String}
         */
        function myDateString(d) {
            if (isDate(d)) {
                /** @type {String[]} */
                var m = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
                /** @type {String} */
                var y = d.getFullYear().toString();
                /** @type {String} */
                var month = m[d.getMonth()];
                return ''.concat(month, '-', y);
            }
            return ''
        }

        /**
         * Formats a number to currency type, eg. 1234567.89 = $1,234,567.89
         * @source http://stackoverflow.com/a/14428340/1618944
         * @param {Number} n
         * @returns {string}
         */
        function toCurrency( n ) {
            return "$".concat(n.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,'));
        }

        /**
         * @namespace MyUtility
         */
        return {
            toNumber: toNumber,
            nsDate: nsDate,
            fDate: fDate,
            fTimeOfDay: fTimeOfDay,
            isDate: isDate,
            sameDates: sameDates,
            sameDateTimes: sameDateTimes,
            zeroPad: zeroPad,
            sDate: sDate,
            getCsvLine: getCsvLine,
            toProperCase: toProperCase,
            myDateString: myDateString,
            toCurrency: toCurrency
        };

    }
);