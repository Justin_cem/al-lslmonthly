# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* A Script to be run monthly to record the correct AL and LSL Balances of Employees in Each Susidiary of ACC

* Version 0.1


### How does it Work ###

* The script performs a saved search to download the following Details:
	* Name
	* Subsidiary
	* Award
	* Years of Service
	* AL Balance
	* LSL Balance
	* Hourly Rate
	
	
* The Script performs another search of Custom Records to download the following Details:
	* Subsidiary
	* AL Expense Account for each different Award
	* LSL Expense Account for each different Award
	* AL Accrual Account for each different Award
	* LSL Accrual Account for each different Award
	
* The Script performs another search of Account Balances to Determine the Accrual Balances for 
	* AL Accrual for each different Award at the End of the Current Month
	* LSL Accrual for each Different Award at the End of the Current Month

* Calculations
	* The Script will Calculate what the AL and LSL Balances for each different Award in each different Subsidiary should be
	* The Script will Calculate if there is any difference between the Calculated Balance and the EOM Balance
	
* Journals
	* The Script will post a Journal if there are differences between the Calculated AL & LSL Balances and the EOM Balance.
	

### Who do I talk to? ###

* Justin Verhoef
* 