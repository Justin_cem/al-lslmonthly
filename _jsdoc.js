/**
 * @typedef {Object} employeeDetails
 * @property {string} employeeName
 * @property {number} hourlyRate
 * @property {number} alHours
 * @property {number} lslHours
 * @property {string} employeeSubsidiary
 * @property {string} employeeDepartment
 * @property {string} employeeAward
 * @property {number} yearsOfService
 * @property {number} availableLSL
 */
/**
 * *@typedef {Object} accountForJournal
 * @property {string} awardName
 * @property {number} awardNumber
 * @property {number} subsidiaryNumber
 * @property {number} departmentNumber
 * @property {number} annualLeaveExpenseAccount
 * @property {number} lslExpenseAccount
 * @property {number} annualLeaveAccrualAccount
 * @property {number} currentLSLAccrualAccount
 * @property {number} nonCurrentLSLAccrualAccount
 */


/**
 * @typedef {Object} accountsList
 * @property {number} alExpenseGeneralEducationAward
 * @property {number} alExpenseIndependentContracts
 * @property {number} alExpenseElcChildServicesAward
 * @property {number} lslExpenseTeachersAward
 * @property {number} lslExpenseGeneralEducationAward
 * @property {number} lslExpenseIndependentContracts
 * @property {number} lslExpenseElcChildServicesAward
 * @property {number} alAccrualTeachersAward
 * @property {number} alAccrualGeneralEducationAward
 * @property {number} alAccrualELCChildServicesAward
 * @property {number} alAccrualIndependentContracts
 * @property {number} lslCurrentAccrualTeachersAward
 * @property {number} lslNonCurrentAccrualTeachersAward
 * @property {number} lslCurrentAccrualGeneralEducationAward
 * @property {number} lslNonCurrentAccrualGeneralEducationAward
 * @property {number} lslCurrentAccrualElcChildServicesAward
 * @property {number} lslNonCurrentAccrualElcChildServicesAward
 * @property {number} lslCurrentAccrualIndependentContracts
 * @property {number} lslNonCurrentAccrualIndependentContracts
 * @property {number} alExpenseElcTeachersAward
 * @property {number} alExpenseElcIndependentContracts
 * @property {number} lslExpenseElcTeachersAward
 * @property {number} lslExpenseElcIndependentContracts
 */

/**
 *@typedef {object} subsidiaryNumber
 *@property {string} name
 *@property {number} internalId
 *@property {string} awardType
 */


/**@typedef {object} accountBalance
  *@property {string} eomBalance
 * @property {number} subsidiaryNumber
 * @property {string} subsidiaryName
 * @property {number} accountNumber
 * @property {string} accountName
 * @property {number} departmentNumber
 * @property {string} departmentName
 */

/**@typedef {object} calculatedAmountForJournal
 * @property {string} subsidiaryName
 * @property {Number} subsidiaryNumber
 * @property {string} departmentName
 * @property {Number} departmentNumber
 * @property {string} awardName
 * @property {number} awardNumber
 * @property {number} alDifference
 * @property {number} nonCurrentLSLDifference
 * @property {number} currentLSLDifference
 * @property {string} alDR/CR
 * @property {string} nonCurrentLSLDR/CR
 * @property {string} currentLSLDR/CR
 */
/**@typedef {object} infoForRecord
 *@property {string} journalNumber
 * @property {Number} subsidiaryNumber
 * @property {Number} departmentNumber
 * @property {Number} awardNumber

 */